<?php

namespace AppBundle\Listeners;

use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use MyNotificationBundle\Mailer\MyMailer;
use AppBundle\Entity\UserNotifications;
use Doctrine\ORM\EntityManager;

class LoginListener {

    protected $userManager;
    protected $mailer;

    public function __construct(UserManagerInterface $userManager, \Swift_Mailer $mailer, EntityManager $em){
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->em = $em;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        //insert notification
        $notification = new UserNotifications();
        $notification->setUserId($user->getId());
        $notification->setMessage('You have log in at ' . date('Y-m-d H:i:s'));
        $this->em->persist($notification);
        $this->em->flush();
        //send email:
        $mailer= new MyMailer($this->mailer);
        $mailer->sendMail($user->getEmail(),  $user->getUsername());
    }
}