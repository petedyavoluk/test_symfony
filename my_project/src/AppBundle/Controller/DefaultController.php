<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\UserNotifications;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        //$this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');
        // replace this example code with whatever you need

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/mark-notification-as-read/{id}", name="mark-notification-as-read")
     */
    public function markNotificationAction($id)
    {
        //get user unread notifications
        $notification = $this->getDoctrine()->getRepository('AppBundle:UserNotifications')->find($id);
        $notification->setIsRead(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($notification);
        $em->flush();
        $url = $this->generateUrl(
            'fos_user_profile_show'
        );
        return new Response(
            '<html><body>Notification is marked as read with id: ' .$id. ' <a href="'.$url.'">back</a></body></html>'
        );
    }

}
