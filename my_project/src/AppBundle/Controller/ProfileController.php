<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\ProfileController as BaseController;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Entity\UserNotifications;

class ProfileController extends BaseController
{
    /**
     * Show the user.
     */
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        //get user unread notifications
        $notifications = $this->getDoctrine()->getRepository('AppBundle:UserNotifications')->findBy(
            array('isRead' => 0, 'userId' =>$user->getId() ),
            array('id' => 'ASC')
        );

        return $this->render('@FOSUser/Profile/show.html.twig', array(
            'user' => $user,
            'notifications' => $notifications,
        ));
    }

}