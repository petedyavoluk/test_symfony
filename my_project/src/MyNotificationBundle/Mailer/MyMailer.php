<?php

namespace MyNotificationBundle\Mailer;

class MyMailer
{
    protected $mailer;
    function __construct($mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendMail($userEmail, $userName){
        //$myfile = fopen("send_mail.txt", "a");
        //fwrite($myfile, 'email to user email: ' . $userEmail . ' and username: ' . $userName . 'was sent');
        //fclose($myfile);

        $body = 'You have logged in';
        $message = \Swift_Message::newInstance()
            ->setSubject('Login notification')
            ->setFrom('noreply@example.com')
            ->setTo($userEmail)
            ->setBody($body)
        ;
        $this->mailer->send($message);
    }
}
